"""
Automated classification of COVID-19 using Convolutional Neural Networks

Authors: Leonardo Rodrigues, Larissa Rodrigues, Danilo Silva, João Mari
"""
# -*- coding: utf-8 -*-

# Import required libraries

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
from torchvision import datasets, models, transforms
import torchvision.datasets as datasets

import os
import random
import numpy as np
from torch.utils.data.sampler import SubsetRandomSampler

import csv
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn import metrics

import seaborn as sns

import time

# Top level data directory. Here we assume the format of the directory conforms 
# to the ImageFolder structure
data_dir = "/home/larissa/Dev/TCC-Leonardo/covid-19/data/"


# Models to choose from [resnet, alexnet, vgg, squeezenet, densenet, inception]
# Escolhe o modelo: resnet, alexnet, vgg, densenet, squeezenet
model_name = "resnet"

# Set number of classes [COVID-19, PNEUMONIA]
num_classes = 2

# Set batch size and num_workers
batch_size = 8
num_workers = 4

# Set epochs of training
num_epochs = 30

# Features extraction (True or False)
#False:  the model is finetuned and all model parameters are updated
#True: only the last layer parameters are updated, the others remain fixed.
feature_extract = True

# Set Learning Rate, Momentum, and Criterion (loss function)
lr = 0.001
momentum = 0.9
criterion = nn.CrossEntropyLoss()


# Data Transforms
# Resize (224x224), Data augmentation, flips and normalization for training
# Just normalization for test
train_transforms = transforms.Compose([
                           transforms.Resize(size=[224,224]),
                           transforms.RandomHorizontalFlip(p=0.5),
                           transforms.RandomVerticalFlip(p=0.5),
			               transforms.RandomRotation(10),
                           transforms.ToTensor(),
                           transforms.Normalize((0.485, 0.456, 0.406),(0.229, 0.224, 0.225))
                       ])

test_transforms = transforms.Compose([
                           transforms.Resize(size=[224,224]),
                           transforms.ToTensor(),
                           transforms.Normalize((0.485, 0.456, 0.406),(0.229, 0.224, 0.225))
                       ])

train_dir = os.path.join(data_dir + 'train/')
valid_dir = os.path.join(data_dir + 'val/')
test_dir = os.path.join(data_dir + 'test/')

# Applying the transformations (test transformation == validation transformation)
train_data = datasets.ImageFolder(train_dir, transform=train_transforms)
valid_data = datasets.ImageFolder(valid_dir, transform=test_transforms) 
test_data = datasets.ImageFolder(test_dir, transform=test_transforms)

# Load the data in parallel using multiprocessing workers through DataLoader iterator
train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True, num_workers=num_workers)

valid_loader = torch.utils.data.DataLoader(valid_data, batch_size=batch_size, shuffle=True, num_workers=num_workers)

test_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, shuffle=True, num_workers=num_workers)

print("----------------------------------------------------------------------------------------")
print('Num training images: ', len(train_data), train_loader)
print('Num valid images: ', len(valid_data), valid_loader)
print('Num test images: ', len(test_data), (test_loader))
print("Num full size of dataset :", len(train_data)+len(valid_data)+len(test_data))
print("----------------------------------------------------------------------------------------")

# Guaranteeing results reproducible for COVID-19 problem on one specific platform and PyTorch version 1.4
SEED = 1234

random.seed(SEED)
np.random.seed(SEED)
torch.manual_seed(SEED)
torch.cuda.manual_seed(SEED)

GPUavailable = torch.cuda.is_available()
if GPUavailable:
    print('Treinamento em GPU!')
    device = torch.device("cuda:0")
else:
    print('Treinamento em CPU!')
    device = "cpu"

torch.backends.cudnn.deterministic = True

# Load a pretrained model and reset final fully connected layer
def set_parameter_requires_grad(model, feature_extracting):
    if feature_extracting:
        for param in model.parameters():
            param.requires_grad = False

# Initializing each specific variable for each CNN model
def initialize_model(model_name, num_classes, feature_extract, use_pretrained=True):
    model_ft = None
    input_size = 0

    if model_name == "resnet":
        """ Resnet18
        """
        model_ft = models.resnet18(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Linear(num_ftrs, num_classes)
        input_size = 224

    elif model_name == "alexnet":
        """ Alexnet
        """
        model_ft = models.alexnet(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.classifier[6].in_features
        model_ft.classifier[6] = nn.Linear(num_ftrs,num_classes)
        input_size = 224

    elif model_name == "vgg":
        """ VGG16
        """
        model_ft = models.vgg11_bn(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.classifier[6].in_features
        model_ft.classifier[6] = nn.Linear(num_ftrs,num_classes)
        input_size = 224

    elif model_name == "densenet":
        """ Densenet
        """
        model_ft = models.densenet121(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.classifier.in_features
        model_ft.classifier = nn.Linear(num_ftrs, num_classes)
        input_size = 224

    elif model_name == "squeezenet":
        model_ft = models.squeezenet1_0(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        model_ft.classifier[1] = nn.Conv2d(512, num_classes, kernel_size=(1,1), stride=(1,1))
        model_ft.num_classes = num_classes
        input_size = 224

    else:
        print("Invalid model name, exiting...")
        exit()

    return model_ft, input_size

# Initialize the model
model, input_size = initialize_model(model_name, num_classes, feature_extract, use_pretrained=True)

# Print the model
print(model)

# Send the model to the GPU
model = model.to(device)

# Gathers the parameters to be optimized/updated in this run
# If adjusted, updates all parameters.
# If feature extraction, update only the initial parameters, i.e., require_grad="True"
params_to_update = model.parameters()
print("Params to learn:")
if feature_extract:
    params_to_update = []
    for name,param in model.named_parameters():
        if param.requires_grad == True:
            params_to_update.append(param)
            print("\t",name)
else:
    for name,param in model.named_parameters():
        if param.requires_grad == True:
            print("\t",name)

# All parameters are being optimized using SGD
optimizer = optim.SGD(params_to_update, lr=lr, momentum=momentum)

# Lists that store the predictions and correct values of training, validation and testing
train_correct_list = []
train_predict_list = []

valid_correct_list = []
valid_predict_list = []

test_correct_list = []
test_predict_list = []

# Support function to create the confusion matrix
def conf_matrix(fx, y, nome):

    if(nome == 'treino'):

      preds = fx.max(1, keepdim=True)[1]
      correct = y

      c = correct.tolist()
      p = preds.flatten().tolist()

      train_correct_list.append(c)
      train_predict_list.append(p)

      return train_correct_list, train_predict_list

    if(nome == 'validacao'):

      preds = fx.max(1, keepdim=True)[1]
      correct = y

      c = correct.tolist()
      p = preds.flatten().tolist()

      valid_correct_list.append(c)
      valid_predict_list.append(p)

      return valid_correct_list, valid_predict_list

    if(nome == 'teste'):

      preds = fx.max(1, keepdim=True)[1]
      correct = y

      c = correct.tolist()
      p = preds.flatten().tolist()

      test_correct_list.append(c)
      test_predict_list.append(p)
      return test_correct_list, test_predict_list

# Function that calculates the accuracy of the training model from the correct predictions
def calculate_accuracy(fx, y):
    preds = fx.max(1, keepdim=True)[1]
    correct = preds.eq(y.view_as(preds)).sum()
    acc = correct.float()/preds.shape[0]

    return acc

# Train function with the previously defined parameters
def train(model, device, iterator, optimizer, criterion, nome):

    epoch_loss = 0
    epoch_acc = 0

    model.train()

    for (x, y) in iterator:

        x = x.to(device)
        y = y.to(device)

        optimizer.zero_grad()

        fx = model(x)

        loss = criterion(fx, y)

        acc = calculate_accuracy(fx, y)

        # Confusion Matrix receives the data and generates the lists (prediction and correct value)
        c, p = conf_matrix(fx, y, nome)

        loss.backward()

        optimizer.step()

        epoch_loss += loss.item()
        epoch_acc += acc.item()

    return epoch_loss / len(iterator), epoch_acc / len(iterator), c, p

# Evaluate model
def evaluate(model, device, iterator, criterion, nome):

    epoch_loss = 0
    epoch_acc = 0

    model.eval()

    with torch.no_grad():
        for (x, y) in iterator:

            x = x.to(device)
            y = y.to(device)

            fx = model(x)

            loss = criterion(fx, y)

            acc = calculate_accuracy(fx, y)

            # Confusion Matrix receives the data and generates the lists (prediction and correct value)
            c, p = conf_matrix(fx, y, nome)

            epoch_loss += loss.item()
            epoch_acc += acc.item()

    return epoch_loss / len(iterator), epoch_acc / len(iterator), c, p

# Creates the folder "./Resultados" to save the results
# Creates the folder "./Resultados/"model_name"
name = './Resultados_Artigo'
if os.path.isdir(name) == False:
    os.mkdir(name)

resultados_dir = './Resultados_Artigo/'+model_name
if os.path.isdir(resultados_dir) == False:
    os.mkdir(resultados_dir)

list_valid_correct = []
list_valid_pred = []
dict_valores = {}

# Train function
def train_function():
  train_losses = []
  val_losses = []

  train_accuracy = []
  val_accuracy = []

  EPOCHS = num_epochs
  SAVE_DIR = 'models_artigo'
  MODEL_SAVE_PATH = os.path.join(SAVE_DIR, model_name+'-coronavirus.pt')

  best_valid_loss = float('inf')
  x = 0

  if not os.path.isdir(f'{SAVE_DIR}'):
      os.makedirs(f'{SAVE_DIR}')

  f = open(resultados_dir+'/'+'resultados.csv', 'w', newline='') # Creates the .csv file
  writer = csv.writer(f)


  writer.writerow( ['Epoch', 'Train Loss', 'Train Acc', 'Val. Loss', 'Val. Acc'] )

  for epoch in range(EPOCHS):

      train_loss, train_acc, train_correct, train_pred = train(model, device, train_loader, optimizer, criterion, 'treino')

      valid_loss, valid_acc, valid_correct, valid_pred = evaluate(model, device, valid_loader, criterion, 'validacao')

      # Epochs list to store the predictions and correct values
      list_valid_correct.append(list.copy(valid_correct))
      list_valid_pred.append(list.copy(valid_pred))

      valid_correct.clear()
      valid_pred.clear()


      if valid_loss < best_valid_loss:
          best_valid_loss = valid_loss
          torch.save(model.state_dict(), MODEL_SAVE_PATH)
          x = epoch
          print("Best Epoch: ", x+1)

      print(f'| Epoch: {epoch+1:02} | Train Loss: {train_loss:.3f} | Train Acc: {train_acc*100:05.2f}% | Val. Loss: {valid_loss:.3f} | Val. Acc: {valid_acc*100:05.2f}% |')
      writer.writerow( [(epoch+1), (train_loss), (train_acc*100), (valid_loss), (valid_acc*100)] ) # Write the data in the .csv file

      train_losses.append(train_loss)
      val_losses.append(valid_loss)

      train_accuracy.append(train_acc)
      val_accuracy.append(valid_acc)

  model.load_state_dict(torch.load(MODEL_SAVE_PATH))

  test_loss, test_acc, test_correct, test_preds = evaluate(model, device, test_loader, criterion, 'teste')


  print("===========================================================================================")
  matriz_confusao(test_correct, test_preds, 'teste')
  print("===========================================================================================")
 
  print(f'| Test Loss: {test_loss:.3f} | Test Acc: {test_acc*100:05.2f}% |')


  # Write the data in the .csv file
  writer.writerow( ['', '', '', '', ''] )
  writer.writerow( ['Test Loss', 'Test Acc', '', '', ''] )
  writer.writerow( [(test_loss), (test_acc*100), '', '', ''])
  f.close() # Fecha aruivo .csv

#   %matplotlib inline
#   %config InlineBackend.figure_format = 'retina'

  # Creates the lost train and validation charts and saves in folder "./Resultados"
  plt.title('Loss Chart')
  plt.plot(train_losses, label='Training loss')
  plt.plot(val_losses, label='Validation loss')
  plt.legend(frameon=False)
  plt.savefig(resultados_dir+'/'+'graficoLoss.png')
  plt.close()
  #plt.show()

  # Creates the accuracy train and validation charts and saves in folder "./Resultados"
  plt.title('Accuracy Chart')
  plt.plot(train_accuracy, label='Training accuracy')
  plt.plot(val_accuracy, label='Validation accuracy')
  plt.legend(frameon=False)
  plt.savefig(resultados_dir+'/'+'graficoAcc.png')
  plt.close()
  #plt.show()

def clear_list():
  train_correct_list.clear()
  train_predict_list.clear()

  valid_correct_list.clear()
  valid_predict_list.clear()

  test_correct_list.clear()
  test_predict_list.clear()

  list_valid_correct.clear()
  list_valid_pred.clear()

# Confusion Matrix
def matriz_confusao(correct, pred, nome):
  correct_list = []
  predict_list = []

  for i in correct:
      correct_list.extend(i)

  for j in pred:
      predict_list.extend(j)

  print("\n")

  print("Matriz de Confusão ("+nome+"): ")
  cm = confusion_matrix(correct_list, predict_list)
  print(cm)


  ax = plt.subplot()
  sns.heatmap(cm, annot=True, cmap="YlGnBu", fmt="d") # annot = True to annotate images
  ax.set_title('Matriz de Confusão ('+nome+')')
  plt.savefig(resultados_dir+'/'+'matrizconfusao_'+nome[:3]+'_.png')
  plt.close()
  #plt.show()

  classes = ['covid','pneumonia']

  print("\nRelatório de classificação ("+nome+"): ")
  report = metrics.classification_report(correct_list, predict_list, target_names=classes)
  print(metrics.classification_report(correct_list, predict_list, target_names=classes))


  file_report = open('./Resultados_Artigo/'+model_name+'/report_matrix.txt', 'a+')
  file_report.write("%s \n" %nome)
  file_report.write(report)
  file_report.write("\n")
  file_report.write("Matriz de Confusão \n")
  file_report.write(str(cm))
  file_report.write("\n \n")
  file_report.close()

  #########################
  # CLASSIFICATION REPORT #
  #########################

  # Calculate the fpr and tpr for all thresholds of the classification
  fpr, tpr, threshold = metrics.roc_curve(correct_list, predict_list)
  roc_auc = metrics.auc(fpr, tpr)

  # method I: plt
  plt.title('Receiver Operating Characteristic')
  plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
  plt.legend(loc = 'lower right')
  plt.plot([0, 1], [0, 1],'r--')
  plt.xlim([0, 1])
  plt.ylim([0, 1])
  plt.ylabel('True Positive Rate')
  plt.xlabel('False Positive Rate')
  plt.savefig(resultados_dir+'/'+'ROC_'+nome[:3]+'_.png')
  plt.close()
  #plt.show()

# Calculate the training time
def calc_time(time_total):
  segundos = time_total

  segundos_rest = segundos % 86400
  horas = segundos_rest // 3600
  segundos_rest = segundos_rest % 3600
  minutos = segundos_rest // 60
  segundos_rest = segundos_rest % 60

  plot_time = "%d hours" %horas+", %d minutes" %minutos+", %d seconds" %segundos_rest
  return plot_time


#########################
# TRAINING
#########################

# Clear the lists of preds and corrects in each execution of the loop
clear_list() 

# Start of training
start = time.time()

# Runing the training function
train_function()

# End of training
end = time.time()
time_total = (end-start)

plot_time = calc_time(time_total)
print(plot_time)

# Write the information in .txt file
tempo_txt = open('./Resultados_Artigo/'+model_name+'/tempo_treinamento.txt', 'a+')
tempo_txt.write(model_name+": ")
tempo_txt.write(plot_time+"\n")
tempo_txt.close()
print("===========================================================================================")
print("\n")