import split_folders

# Split with a ratio.
# To only split into training and validation set, set a tuple to `ratio`, i.e, `(.8, .2)`.
split_folders.ratio('/home/larissa/Dev/TCC-Leonardo/covid-19/data/', output="output", seed=42, ratio=(.8, .1, .1)) # default values
print("Splits Ok!")
