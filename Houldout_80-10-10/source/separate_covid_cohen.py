import pandas as pd
import os, shutil

metadata_path='covid-chestxray-dataset/metadata.csv'
df=pd.read_csv(metadata_path)

#types we're interested in
covid_patients=df['finding']=='COVID-19'
CT=df['view']=='CT'
PA=df['view']=='PA'

# %%
df[covid_patients & CT].shape
df[covid_patients & PA].shape
# %%
PA_covid=df[covid_patients & PA]
Others=df[~covid_patients & PA]
covid_files=[files for files in PA_covid['filename']]
other_files=[files for files in Others['filename']]

#our test folder. manually included files via upload.
test_files=[file for file in sorted(os.listdir('data/test'))]
df_test=pd.DataFrame(test_files, columns=['filename'])

#create data folder and positive & negative cases folder, and test folder
destpath = 'data/covid','data/other', 'data/test'
srcpath = 'covid-chestxray-dataset/images'

for root, dirs, files in os.walk(srcpath):
  if not os.path.isdir(destpath[0]):
    os.makedirs(destpath[0])
  if not os.path.isdir(destpath[1]):
    os.makedirs(destpath[1])
  if not os.path.isdir(destpath[2]):
    os.makedirs(destpath[2])
  for file in files:
    if file in covid_files:
      shutil.copy(os.path.join(root, file),destpath[0])
    if file in other_files:
      shutil.copy(os.path.join(root,file),destpath[1])
    if file in test_files:
      shutil.copy(os.path.join(root,file)),destpath[2]

#see number of files
path, dirs, files2 = os.walk("data/other").__next__()
path, dirs, files1 = os.walk("data/covid").__next__()
path, dirs, files3 = os.walk("data/test").__next__()
print("Number of images in Other: {}".format(len(files2)),"Number of images in Covid: {}".format(len(files1)),"Number of images in Test Set: {}".format(len(files3)) )