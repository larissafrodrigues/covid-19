from PIL import Image
import os, sys

path = "/home/larissa/Dev/TCC-Leonardo/covid-19/data/other/"
dirs = os.listdir( path )

def resize():
    for item in dirs:
        if os.path.isfile(path+item):
            im = Image.open(path+item)
            f, e = os.path.splitext(path+item)
            rgb_im = im.convert('RGB')
            imResize = rgb_im.resize((224,224), Image.ANTIALIAS)
            imResize.save(f + '_224x224.jpeg')
print("OK")
resize()

