import argparse as ap
import cv2
import numpy as np
import os, time, shutil

from sklearn import (cluster, preprocessing, svm, externals, neural_network,
                     model_selection, metrics)


def copy_files(P_list, y_list, round_, split):
    """
    """
    print('path \t y ')
    i=0
    for path, y_ in zip(P_list, y_list):
        print(str(i) + '\t' + y_ )
        print(path)
        i+=1

        # 'path_out' : pasta principal do conjunto de dados
        # Ex.: '../data/'
        path_temp = path.split('/')[:-4]
        path_out = ''
        for path_temp_ in path_temp:
            path_out = path_out + path_temp_ + '/'
        ### print(path_out)

        # Classe da imagem.
        # Ex.: 'COVID/'
        # class_temp = path.split('/')[-2:-1]
        path_class = path.split('/')[-2]
        ### print(path_class)

        # Preprocessamento aplicado.
        path_preproc = path.split('/')[-3]
        ### print(path_preproc)

        # Caminho de saída sem o nome do arquivo.
        # Ex. : '../data/<nome_do_ds>/<K>-fold/<preprocessamento>/round_<k>/<split>/<classe>/'
        # Ex. : '../data/fep-2/5-fold/contrast/round_2/train/Homogeneous/'
        path_out = path_out + str(n_splits) + '-fold/' + path_preproc + '/round_' + str(round_) + '/' + split + '/' + path_class + '/'
        if not os.path.exists(path_out):
            os.makedirs(path_out)

        # path_out com nome do arquivo no final.
        path_out = path_out + path.split('/')[-1]
        print(path_out)

        # Copiar de path para path_out
        shutil.copy2(path, path_out)
# -----------------------------------------------------------------------------

if __name__ == '__main__':
   

    ds_path = './dataset/' 
    print(ds_path)

    # List of paths for all files.
    path_list = []
    # List of label for each file.
    y         = []

    # K number in k-fold.
    n_splits = 5

    # Lista ordenada de classes
    classes = os.listdir(ds_path)
    classes.sort()

    # Iterate along each class (folder)
    for class_ in classes:
        # Lista ordenada dos arquivos (imagens) no folder atual
        path_list_ = os.listdir(ds_path + '/' + class_)
        path_list_.sort()

        # Iterate along the files in path_list
        for path_image in path_list_:
            path_list.append(ds_path + class_ + '/' + path_image)
            y.append(class_)

    # **** TEST ****
    print('path_list')
    print(len(path_list))

    # Gera os conjuntos de treinos e de teste
    print('\nSeparando conjunto de TREINO e TESTE...')

    '''
    # Não usar train_test_split, pois não é estratificado.
    P_kfold, P_test, y_kfold, y_test = \
            model_selection.train_test_split(path_list, y,
                                             test_size=(1./(n_splits + 1)),
                                             random_state=42)
    '''

    """
    sss = model_selection.StratifiedShuffleSplit(n_splits=1,
                                 test_size=(1./(n_splits + 1)),
                                 random_state=42)

    y = np.asarray(y)

    for kfold_index, test_index in sss.split(path_list, y):
        # Caminhos para os arquivos: k-Fold / Test
        P_kfold = [path_list[i] for i in kfold_index]
        P_test  = [path_list[i] for i in test_index]
        # Rotulos: Treino / Validação
        y_kfold, y_test = y[kfold_index], y[test_index]
    """

    y = np.asarray(y)

    # Stratified K-Fold
    # -----------------
    skf = model_selection.StratifiedKFold(n_splits=n_splits, random_state=42)

    """
    #P_kfold = np.asarray(P_kfold)
    #D_kfold = np.asarray(D_kfold)
    y_kfold = np.asarray(y_kfold)
    """

    # =========================================================================
    # K-fold
    # -------------------------------------------------------------------------
    # Counter for the k-fold round.
    round_count = 0

    for train_index, val_index in skf.split(path_list, y):

        # Update the round (k-fold) counter.
        round_count += 1

        # Caminhos para os arquivos: Treino / Validação
        P_train = [path_list[i] for i in train_index]
        P_val   = [path_list[i] for i in val_index]
        # Rotulos: Treino / Validação
        y_train, y_val = y[train_index], y[val_index]

        print('\nFold ' + str(round_count) + '\n====================')

        print('\nTrainnig \n --------------------')
        copy_files(P_train, y_train, round_count, 'train')
        print('\nValidation \n --------------------')
        copy_files(P_val, y_val, round_count, 'val')
        """
        print('\nTest \n --------------------')
        copy_files(P_test, y_test, round_count, 'test')
        """

    """
    # Test - K=6
    # Training
    # ----------
    print('\nFold '+ str(n_splits+1) + '\n ====================')

    print('\nTrainnig \n --------------------')
    copy_files(P_kfold, y_kfold, n_splits+1, 'train')
    print('\nValidation \n --------------------')
    copy_files(P_test, y_test, n_splits+1, 'val')
    print('\nTest \n --------------------')
    copy_files(P_test, y_test, n_splits+1, 'test')
    """

