## Deep Learning at chest radiography images: 

## Automated classification of COVID-19 using Convolutional Neural Networks

Paper submited to Clinical Radiology

## Authors
Leonardo Gabriel Ferreira Rodrigues <leonardo.g.rodrigues@ufv.br>

Larissa Ferreira Rodrigues <larissa.f.rodrigues@ufv.br>

Danilo Ferreira da Silva <danilo.f.silva@ufv.br>

João Fernando Mari <joaof.mari@ufv.br>

## Highlights
- To compare different CNN models to identify COVID-19 in chest radiography images.

- Fine-tuning training using transfer learning due to the lack of a large database.

- SqueezeNet deep CNN has the best diagnostic accuracy with AUC of 0.984.

- The proposed method could help healthcare workers improve COVID-19 diagnosis.

## Image dataset
| COVID-19 | Sample |                                  Source                                                             |
|:--------:|:------:|:---------------------------------------------------------------------------------------------------:|
| Positive |   76   | [GitHub (Dr. Joseph Cohen)](https://github.com/ieee8023/covid-chestxray-dataset)                    |
| Negative |   299  | [Kaggle (X-ray images of Pneumonia)](https://www.kaggle.com/paultimothymooney/chest-xray-pneumonia) |
|   Total  |   375  |                                                                                                     |

## Technical Details
- Python 3.6
- PyTorch 1.4